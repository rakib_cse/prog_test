"""
Description: Get the title from the given URL page and saves it to the out.txt file
             and find the total lowercase, uppercase, and spaces in that title.
Output: out.txt file
"""

import os
import sys
try:
    from requests_html import HTMLSession
except ImportError:
    os.system("python -m pip install -r requirements.txt")
    print("Please re-run the script.")
    sys.exit(1)

import re

# OUTPUT
output_filename = "out.txt"


def get_title(temp_url):
    """
    Return temp_title of the page
    Args:
        temp_url (str):

    Returns:
        temp_title (str):
    """

    session = HTMLSession()

    try:
        page_response = session.get(temp_url)
        if page_response.status_code == 200:
            temp_title = page_response.html.find("title", first=True).text
            return temp_title
        else:
            print("Error getting the page response")
    except Exception as err:
        print(err)


if __name__ == '__main__':

    url = "https://europa.eu/european-union/about-eu/countries_en"

    title = get_title(url)

    with open(output_filename, "w") as file:
        file.write(title)

    lowercase_letters = "".join(re.findall(r"[a-z]*", title))
    uppercase_letters = "".join(re.findall(r"[A-Z]*", title))
    spaces = "".join(re.findall(r"\s*", title))

    print("""
        Total lowercase characters: {},
        Total uppercase characters: {},
        Total spaces: {}
    """.format(len(lowercase_letters), len(uppercase_letters), len(spaces)))
